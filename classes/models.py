from django.db import models

# Create your models here.
class Classes(models.Model):
    matkul = models.CharField(max_length=45)
    dosen = models.CharField(max_length=75)
    sks = models.CharField(max_length=2)
    desc = models.TextField()
    semester = models.CharField(max_length=45)
    ruang = models.CharField(max_length=45)

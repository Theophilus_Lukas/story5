from django.urls import path

from . import views
from .views import ClassesView, ClassesDetailView, ClassesDeleteView

app_name = 'classes'

urlpatterns = [
    path('', ClassesView.as_view(), name='list'),
    path('detail/<int:pk>', ClassesDetailView.as_view(), name='classes-detail'),
    path('delete/<int:pk>', ClassesDeleteView.as_view(), name='classes-delete'),
    path('add/', views.add, name='add'),
]
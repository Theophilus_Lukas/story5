from django.shortcuts import render
from django.views.generic import ListView, DetailView, DeleteView
from .forms import ClassesForm
from .models import Classes
from django.urls import reverse_lazy

# Create your views here.

class ClassesView(ListView):
    model = Classes
    template_name = 'classes/list.html'

class ClassesDetailView(DetailView):
    model = Classes
    template_name = 'classes/detail.html'

class ClassesDeleteView(DeleteView):
    model = Classes
    template_name = 'classes/delete.html'
    success_url = reverse_lazy('classes:list')

def add(request):
    form = ClassesForm(request.POST or None)
    if form.is_valid():
        form.save()
    context = {
        'form': form
    }
    return render(request, 'classes/add.html', context)
